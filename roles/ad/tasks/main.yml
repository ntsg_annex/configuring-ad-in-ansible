---

# ------------------------------------------------------------------------
# UPDATE SOFTWARE AND INSTALL PACKAGES NEEDED
# ------------------------------------------------------------------------

- name: "Update system software"
  yum: name=* state=latest

- name: "Install packages needed for active directory, samba, keroberos, and sssd"
  yum: 
    name: "{{ item }}" 
    state: latest
  with_items:
    - adcli
    - realmd
    - samba
    - samba-common
    - sssd
    - authconfig
    - chrony

- name: "Install pip"
  easy_install: name=pip

- name: "Install pexpect using pip"
  pip: name=pexpect

# ------------------------------------------------------------------------
# CHANGE TIMEZONE (IF IT'S NOT ALREADY SET CORRECTLY)
# ------------------------------------------------------------------------

- name: Check current timezone before changing it
  stat: path=/etc/localtime
  register: current_zone
  always_run: yes

- name: "Set timezone to {{ timezone }}"
  file: 
    src: "/usr/share/zoneinfo/{{ timezone }}"
    dest: /etc/localtime 
    owner: root 
    group: root 
    state: link 
    force: yes
  when: (not current_zone.stat.exists or (current_zone.stat.islnk is defined and not current_zone.stat.islnk)) or (current_zone.stat.islnk is defined and current_zone.stat.islnk and current_zone.stat.lnk_source.match(timezone))

# ------------------------------------------------------------------------
# CONFIGURE CHRONY
# ------------------------------------------------------------------------

- name: "If desired, we remove the the default ntp server pools in the chrony conf file. The original file is backed up."
  lineinfile: 
    dest: "/etc/chrony.conf"
    backup: yes
    regexp: '^server (?!({{ ntp_servers | join("|", attribute="addr") }}))'
    owner: root 
    group: root
    state: absent
  when: use_default_ntp_servers != true
  notify: restart chronyd

- name: "We add in our own ntp server/s in the chrony conf file. The original file is backed up."
  lineinfile: 
    dest: "/etc/chrony.conf"
    backup: yes
    owner: root 
    group: root
    insertbefore: BOF
    line: "server {{ item.addr }}"
  with_items: "{{ ntp_servers }}"
  when: use_default_ntp_servers != true
  notify: restart chronyd

- name: "Enable the chrony daemon service to run on boot."
  service: name=chronyd enabled=yes

- name: "Start the chrony daemon service"
  service: name=chronyd state=started

# ------------------------------------------------------------------------
# CONFIGURE SSH
# ------------------------------------------------------------------------

- name: "Remove unwanted SSH options in /etc/ssh/sshd_config. The original file is backed up."
  lineinfile:
    dest: "/etc/ssh/sshd_config"
    backup: yes
    regexp: "{{ item.regexp }}"
    owner: root 
    group: root
    state: absent
  with_items:
    - { regexp: "^KerberosAuthentication (?!yes)" }
    - { regexp: "^PasswordAuthentication (?!yes)" }
  notify: restart sshd

- name: "Edit /etc/ssh/sshd_config to allow password logins and kerberos authentication. The original file is backed up."
  lineinfile: 
    dest: "/etc/ssh/sshd_config"
    backup: yes
    owner: root 
    group: root
    line: "{{ item.line }}"
    insertafter: "{{ item.insertafter }}"
  with_items:
    - { line: "KerberosAuthentication yes", insertafter: "^# Kerberos options" }
    - { line: "PasswordAuthentication yes", insertafter: "^#PermitEmptyPasswords" }
  notify: restart sshd

- meta: flush_handlers

# ------------------------------------------------------------------------
# CONFIGURE REALMD
# ------------------------------------------------------------------------
- name: "Configure /etc/realmd.conf"
  template: src=realmd.conf.fedora7.j2 dest=/etc/realmd.conf owner=root group=root mode=0600

# ------------------------------------------------------------------------
# JOIN MACHINE TO AD DOMAIN
# ------------------------------------------------------------------------

- name: "Check if {{ ansible_hostname }} is joined to AD domain, {{ ad_domain.upper() }}"
  command: /bin/bash -c "/usr/sbin/realm list"
  register: joined_domains
  always_run: yes
  changed_when: no

- name: "Join {{ ansible_hostname }} to AD domain, {{ ad_domain.upper() }}"
  expect:
    command: /bin/bash -c "/usr/sbin/realm join --membership-software=adcli --user={{ vault_ad_user }} {{ ad_domain.upper() }}"
    responses:
      (?i)password: "{{ vault_ad_pass }}"
  when: ad_domain.lower() not in joined_domains.stdout

# ------------------------------------------------------------------------
# ALLOW ONLY A SPECIFIED LIST OF GROUPS TO LOGIN
# ------------------------------------------------------------------------

- name: "Ensure that that metadata file for permitting ad groups is present"
  file:
    path: "{{ playbook_metadata_dir }}/{{ ad_domain.lower() }}_ad_allow"
    state: touch
  changed_when: no

- name: "Check if we have already added these groups"
  command: /bin/bash -c "/bin/cat {{ playbook_metadata_dir }}/{{ ad_domain.lower() }}_ad_allow"
  register: ad_allow_metadata
  always_run: yes
  changed_when: no
  ignore_errors: yes

- name: "Register the difference between the groups that have been added and the groups listed in the playbook"
  set_fact:
    realmd_groups_to_add: "{{ realmd_allowed_groups|map(attribute='group_name')|list|difference(ad_allow_metadata.stdout_lines)|list }}"
  always_run: yes

- name: "Deny all users and groups to log in to AD domain"
  command: /usr/sbin/realm deny -R {{ ad_domain.lower() }} --all
  when: (realmd_allowed_groups | length > 0) and (realmd_groups_to_add | length > 0)

- name: "Allow certain groups to log in to AD domain"
  command: /usr/sbin/realm permit -R {{ ad_domain.lower() }} -g {{ item }}
  with_items: "{{ realmd_groups_to_add }}"
  when: (realmd_allowed_groups | length > 0) and (realmd_groups_to_add | length > 0)

- name: "Register the metadata file for allowing ad groups"
  lineinfile:
    dest: "{{ playbook_metadata_dir }}/{{ ad_domain.lower() }}_ad_allow"
    regexp: "^{{ item }}"
    line: "{{ item }}"
  with_items: "{{ realmd_groups_to_add }}"
  when: (realmd_allowed_groups | length > 0) and (realmd_groups_to_add | length > 0)

# ------------------------------------------------------------------------
# ALLOW ONLY A SPECIFIED LIST OF GROUPS TO LOGIN
# ------------------------------------------------------------------------

- name: "Ensure the list of groups for sudo exist"
  group:
    name: "{{ item.group_name }}"
    state: present
  with_items: "{{ ad_sudoer_groups }}"
  when: ad_sudoer_groups | length > 0

- name: "Add the comment marker for the beginning of the ansible block for the sudoers file."
  lineinfile:
    dest: "/etc/sudoers"
    backup: yes
    regexp: "^{{ ad_sudoer_comment_begin_block_regex }}"
    line: "{{ ad_sudoer_comment_begin_block }}"
    state: present
    validate: "visudo -cf %s"
  when: ad_sudoer_groups|length > 0

- name: "Add the comment marker for the end of the ansible block for the sudoers file."
  lineinfile:
    dest: "/etc/sudoers"
    backup: yes
    regexp: "^{{ ad_sudoer_comment_end_block_regex }}"
    line: "{{ ad_sudoer_comment_end_block }}"
    insertafter: "^{{ ad_sudoer_comment_begin_block_regex }}"
    validate: "visudo -cf %s"
  when: ad_sudoer_groups|length > 0

- name: "Add the given list of groups to have sudo. The sudoers file is backed up."
  lineinfile:
    dest: "/etc/sudoers"
    backup: yes
    regexp: "^%{{ item.group_name }}"
    line: "%{{ item.group_name }} {{ ad_sudoer_permission_string }}"
    insertafter: "^{{ ad_sudoer_comment_begin_block_regex }}"
    validate: "visudo -cf %s"
  with_items: "{{ ad_sudoer_groups }}"
  when: ad_sudoer_groups | length > 0
